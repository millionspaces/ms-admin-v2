import logo200Image from 'assets/img/logo/logo_200.png';
import React from 'react';
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';

class AuthForm extends React.Component {

  state = {
    userName : "",
    password : ""
  }

  handleSubmit = event => {
    event.preventDefault();
    const {password, userName} = this.state;

    if(userName && password){
      this.props.handleLoginSubmit({
        userName  : userName,
        password  : password
      });
    }
  };

  handleOnChange = (e) => {
    this.setState({
      [e.target.name] : e.target.value
    });
  }

  render() {
    const {
      showLogo,
      usernameLabel,
      passwordLabel,
      children,
      onLogoClick,
    } = this.props;
    const {password, userName} = this.state;

    return (
      <Form >
          <div className="text-center pb-4">
            <img
              src={logo200Image}
              className="rounded"
              style={{ width: 60, height: 60, cursor: 'pointer' }}
              alt="logo"
              onClick={onLogoClick}
            />
          </div>

        <FormGroup>
          <Label for={usernameLabel}>{usernameLabel}</Label>
          <Input name={"userName"} value={userName} onChange={this.handleOnChange} placeholder={"User Name"}/>
        </FormGroup>
        <FormGroup>
          <Label for={passwordLabel}>{passwordLabel}</Label>
          <Input type="password" name={"password"} value={password}  onChange={this.handleOnChange} placeholder={"Password"}/>
        </FormGroup>
      
        <hr />
        <Button
          size="lg"
          className="bg-gradient-theme-left border-0"
          block
          onClick={this.handleSubmit}>
          {"Login"}
        </Button>

        {children}
      </Form>
    );
  }
}

export default AuthForm;
