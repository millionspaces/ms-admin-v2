import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';

import registerServiceWorker from './registerServiceWorker';

// Redux stuff
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import reducers from './redux/reducers';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

// Other
import { CookiesProvider } from 'react-cookie';
import immutableCheckMiddleWare from 'redux-immutable-state-invariant';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducers, composeEnhancers (
    applyMiddleware (thunk, logger, immutableCheckMiddleWare())
));


ReactDOM.render(<Provider store={store}>
    <CookiesProvider><App /></CookiesProvider>
</Provider>, document.getElementById('root'));
