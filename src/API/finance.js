import { API_SETTINGS } from './CONSTS';
const { API } = API_SETTINGS;

/**
 * API Call: Get finance host details.
 */

export const fetchFinanceHostPayments = (page) => {
    return fetch(`${API}admin/booking/finance/${page}`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        }
    }).then(res=>res.json());
};


