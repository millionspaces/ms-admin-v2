/**
 * This file must contain API related settings only.
 */

import { api } from '../settings';

export const API_SETTINGS = {
    API: api
}