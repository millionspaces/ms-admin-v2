import { API_SETTINGS } from './CONSTS';

const { API } = API_SETTINGS;

/**
 * API Call: Fetch managed bookings.
 */

export const fetchManagedBookingsByPage = pageNumber => {
    return fetch (`${API}admin/booking/new/${pageNumber}`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        }
    });
}
/**
 * get manageBookings for id
 * @param {id} id 
 */
export const getManageBookingsByReferenceId = id => {
    return fetch(`${API}admin/booking/detail/${id}`, {
        method:  'GET',
        credentials: 'include',
        headers: {
            'content-type' : 'application/json'
        }
    })
};

export const fetchABooking = bookingId => {
    return fetch (`${API}book/spaces`, {
        method: 'POST',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify({
            isManual: false,
            bookingId
        })
    });
}

export const cancelBooking = bookingId => {
    return fetch(`${API}book/space`, {
        method: 'PUT',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify({
            booking_id: bookingId,
            event: 3
        })
    })
}