import { API_SETTINGS } from './CONSTS';

const { API } = API_SETTINGS;

/**
 * API Call: Get all transaction detals.
 * @param {Integer} pageId Page id
 */
export const fetchTransactionDetails = pageId => {
    return fetch (`${API}admin/booking/paid/${pageId}`,  {
        method: 'GET',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        }
    });
}

export const markTransactionReceived = pdf => {
    return fetch (`${API}admin/booking/payment`, {
        method: 'POST',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify(pdf)
    });
}

export const fetchBybookingId = bookingId => {
    return fetch (`${API}admin/booking/payment/${bookingId}`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            'content-type': 'application/json'
        }
    }).then(res=>res.json());
}