import { API_SETTINGS } from './CONSTS';

const { API } = API_SETTINGS;

/**
 * Saves Array of images of a space
 * @param {Array} imageData contains array of objects explaining images
 */
export const saveSpaceImages = (imageData) => {
    return fetch (`${API}space/images`, {
        method: 'POST',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify (imageData)
    });
};

/**
 * Get image data.
 */

 export const fetchImageData = image => {
     return fetch (`${API}space/images/${image}`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        }
    }); 
 };

 /**
  * Delete Image from cloudinery
  */
  export const deleteImageFromCloudinery = (token) => {
      return fetch (`https://api.cloudinary.com/v1_1/dz4u73trs/delete_by_token`,  {
        method: 'POST',
        headers: {
            "content-type": "application/json",
            'X-Requested-With': 'XMLHttpRequest'
        },
        body: JSON.stringify({token})
      });
  };