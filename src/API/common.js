// TODO: REFACT DOC

import { API_SETTINGS } from './CONSTS';

const { API } = API_SETTINGS;


/**
 * API Call: Requesting all measurement unit.
 */
export const fetchMeasurementUnits = () => {
    return fetch(`${API}common/measurementUnit`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        }
    }).then(res=>res.json());
};

/**
 * API Call: Requesting seating arrangements.
 */
export const fetchAllSeatingArrangements = () => {
    return fetch(`${API}common/seatingArrangement`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        }
    }).then(res=>res.json());
};

/**
 * API Call: Requesting all event types.
 */
export const fetchAllEventTypes = () => {
    return fetch(`${API}common/eventTypes`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        }
    }).then(res=>res.json());
};

/**
 * API Call: Requesting all amenities.
 */
 export const fetchAllAmenities = () => {
    return fetch(`${API}common/amenities`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        }
    }).then(res=>res.json());
 };

/**
 * API Call: Requesting amenity units.
 */
export const fetchAmenityUnits = () => {
    return fetch(`${API}common/amenityUnits`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        }
    }).then(res=>res.json());
 };

/**
 * API Call: Requesting All charge types
 */
export const fetchAllChargeTypes = () => {
    return fetch(`${API}common/chargeTypes`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            'content-type': 'application/json'
        }
    }).then(res=>res.json());
}


/**
 * API Call: Requesting Block charge types
 */
export const fetchBlockChargeTypes = () => {
    return fetch(`${API}common/blockChargeTypes`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            'content-type': 'application/json'
        }
    }).then(res=>res.json());
}

/**
 * API Call: Fetching space types
 */

export const fetchSpaceTypes = () => {
    return fetch(`${API}common/spaceTypes`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            'content-type': 'application/json'
        }
    }).then(res=>res.json());
}



/**
 * API Call: Update Space rules.
 * @param {Object} space 
 */
export const fetchSpaceRules = space => {
    return fetch (`${API}common/rules`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            'content-type': 'application/json'
        }
    }).then(res=>res.json());
}


/**
 * API Call: Cancellation policy
 */
export const fetchCancellationPolicies = space => {
    return fetch (`${API}common/cancellationPolicies`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            'content-type': 'application/json'
        }
    }).then(res=>res.json());
}

// export const removeImageFromCloudinary = () => {
//     return fetch (`https://api.cloudinary.com/v1_1/dz4u73trs/delete_by_token`, {
//         method: 'POST',
//         headers: {
//             "content-type": "application/x-www-form-urlencoded"
//         },
//         body:
//     })
// }
