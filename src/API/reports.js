import { API_SETTINGS } from './CONSTS';
const axios = require('axios');

const { API } = API_SETTINGS;

export const getReportDetails = (path) => {
    return axios.get(`${API}common/reports?procedureName=${path}`).then((res) =>{
        return res;
    }).catch(error => 
        error);
}

export const getBookingReportDetails = (path, startOfWeek, endOfWeek) => {
    return axios.get(`${API}common/reports?procedureName=${path}('${startOfWeek}','${endOfWeek}')`).then((res) =>{
        return res;
    }).catch(error => 
        error);;
}

export const getUserReportDetails = (path, startDate, endDate) => {
    return axios.get(`${API}common/reports?procedureName=${path}('${startDate}','${endDate}')`).then((res) =>{
        return res;
    }).catch(error => 
        error);;
}

