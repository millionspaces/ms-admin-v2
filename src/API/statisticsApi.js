import { API_SETTINGS } from './CONSTS';
const axios = require('axios');

const { API } = API_SETTINGS;

export const getUsersStatistics = (startDate, endDate) => {
    return axios.get(`${API}common/reports?procedureName=${'user_summary_report'}('${startDate}','${endDate}')`).then((res) =>{
        return res;
    }).catch(error => 
        error);
};