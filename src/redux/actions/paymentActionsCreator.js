import { TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS, TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS_FAILED, TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS_SUCCESS, TENTETIVE_BOOKINGS___CONFIRMING_TENTETIVE_BOOKING, TENTETIVE_BOOKINGS___CONFIRMING_TENTETIVE_BOOKING_SUCCESS, TENTETIVE_BOOKINGS___CONFIRMING_TENTETIVE_BOOKING_FAILED } from "../actionTypes/paymentsActionTypes";
import { fetchTentetiveBooking, confirmTentetiveBooking, fetchByBookingId } from "../../API/payments.api";

export const getTentetiveBookings = pageNumber => {
    return dispatch => {
        dispatch ({
            type: TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS
        });

        fetchTentetiveBooking (pageNumber)
            .then (res => res.json())
            .then (data => {
                dispatch ({
                    type: TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS_SUCCESS,
                    payload: data
                });
            })
            .catch (err => {
                dispatch ({
                    type: TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS_FAILED,
                    payload: err
                });
            })
    }
};

export const getTentetiveBookingsbyBookingId = bookingId => {
    return dispatch => {
        dispatch ({
            type: TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS
        });

        fetchByBookingId(bookingId)
        .then( res => {
            if(res.code && res.code === 500){
                dispatch ({
                    type: TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS_SUCCESS,
                    payload: {bookings : [] }
                });
            }else{
                dispatch ({
                    type: TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS_SUCCESS,
                    payload: {bookings : [res] }
                });
            }
        })
        .catch (err => {
            dispatch({
                type: TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS_FAILED,
                payload: err
            })
        });
    }

}

export const confirmTentetiveBookingActionCreator = bookingId => {
    return dispatch => {
        dispatch ({
            type: TENTETIVE_BOOKINGS___CONFIRMING_TENTETIVE_BOOKING
        });

        confirmTentetiveBooking (bookingId)
            .then (res => res.json())
            .then (data => {
                
                dispatch ({
                    type: TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS
                });
        
                fetchTentetiveBooking (0)
                    .then (res => res.json())
                    .then (data => {
                        dispatch ({
                            type: TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS_SUCCESS,
                            payload: data
                        });

                        dispatch ({
                            type: TENTETIVE_BOOKINGS___CONFIRMING_TENTETIVE_BOOKING_SUCCESS,
                            payload: data
                        });
                    })
                    .catch (err => {
                        dispatch ({
                            type: TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS_FAILED,
                            payload: err
                        });
                    })
            })
            .catch (err => {
                dispatch ({
                    type: TENTETIVE_BOOKINGS___CONFIRMING_TENTETIVE_BOOKING_FAILED,
                    payload: err
                })
            });
    }
};