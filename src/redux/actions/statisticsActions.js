
import { SPACE_STATISTICS_FETCHING, SPACE_STATISTICS_FETCH_SUCCESS, SPACE_STATISTICS_FETCH_FAIL} from '../actionTypes/statisticsActionTypes';
import {getUsersStatistics} from '../../API/statisticsApi';

export const getUserStatistics = (startDate, endDate) => {
    return dispatch => {
        dispatch({
            type : SPACE_STATISTICS_FETCHING
        });

        getUsersStatistics(startDate, endDate)
        .then(res => {
            dispatch({
                type    : SPACE_STATISTICS_FETCH_SUCCESS,
                payload : {user : res.data}
            })
        })
        .catch(error => {
            dispatch({
                type    : SPACE_STATISTICS_FETCH_FAIL,
                payload : error
            })

        });
    }
}