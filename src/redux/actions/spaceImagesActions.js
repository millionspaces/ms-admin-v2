import { SPACE_IMAGES__SAVE_SPACE_IMAGES, SPACE_IMAGES__SAVE_SPACE_IMAGES_SUCCESS, SPACE_IMAGES__SAVE_SPACE_IMAGES_FAILED, SPACE_IMAGES__FETCHING_SPACE_IMAGE_DATA, SPACE_IMAGES__FETCHING_SPACE_IMAGE_DATA_SUCCESS, SPACE_IMAGES__FETCHING_SPACE_IMAGE_DATA_FAILED, SPACE_IMAGES__REMOVING_SPACE_IMAGE_FROM_CLOUD, SPACE_IMAGES__REMOVING_SPACE_IMAGE_FROM_CLOUD_SUCCESS, SPACE_IMAGES__REMOVING_SPACE_IMAGE_IN_SPACES_IMAGES, SPACE_IMAGES__REMOVING_SPACE_IMAGE_IN_SPACES_IMAGES_SUCCESS, SPACE_IMAGES__REMOVING_SPACE_IMAGE_IN_SPACES_IMAGES_FAILED } from "../actionTypes/spaceImagesActionTypes";
import { saveSpaceImages, fetchImageData, deleteImageFromCloudinery } from "../../API/images.api";

/**
 * Creates actions be dispatched explaining image details.
 * @param {Object} imageDetails contains array of objects explaining image details.
 */
export const setSpaceImagesDetails = (imageDetails) => {
    return dispatch => {

        dispatch ({
            type: SPACE_IMAGES__SAVE_SPACE_IMAGES
        });

        saveSpaceImages (imageDetails)
            .then (res => res.json())
            .then (data => {
                dispatch ({
                    type: SPACE_IMAGES__SAVE_SPACE_IMAGES_SUCCESS,
                    payload: data
                });
            })
            .catch (err => {
                dispatch ({
                    type: SPACE_IMAGES__SAVE_SPACE_IMAGES_FAILED,
                    payload: err
                });
            })
    }
};

/**
 * Creates actions for fetching image data.
 * @param {String} url 
 */
export const getSpaceImageData = (url) => {
    return dispatch => {

        dispatch ({
            type: SPACE_IMAGES__FETCHING_SPACE_IMAGE_DATA
        });

        fetchImageData (url)
            .then(res => res.json())
            .then( data => {
                dispatch ({
                    type: SPACE_IMAGES__FETCHING_SPACE_IMAGE_DATA_SUCCESS,
                    payload: data
                });
            })
            .catch (err => {
                dispatch ({
                    type: SPACE_IMAGES__FETCHING_SPACE_IMAGE_DATA_FAILED,
                    payload: err
                });
            });
        
    }
}

export const removeImageFromCloudinery = (token) => {

    return dispatch => {

        dispatch ({
            type: SPACE_IMAGES__REMOVING_SPACE_IMAGE_FROM_CLOUD
        });

        deleteImageFromCloudinery (token)
            .then (res => res.json())
            .then (data => {
                dispatch ({
                    type: SPACE_IMAGES__REMOVING_SPACE_IMAGE_FROM_CLOUD_SUCCESS,
                    payload: data
                });
            })
            .catch (err => {
                dispatch ({
                    type: SPACE_IMAGES__FETCHING_SPACE_IMAGE_DATA_FAILED,
                    payload: err
                });
            });
    }
}

export const getImgDetailsAndRemoveImageFromCloudinery = url => {

    return dispatch => {

        dispatch ({
            type: SPACE_IMAGES__REMOVING_SPACE_IMAGE_IN_SPACES_IMAGES
        });

        fetchImageData (url)
            .then (res => res.json())
            .then (data => {

                console.log(data)

                deleteImageFromCloudinery (data.deleteToken)
                    .then (res => res.json())
                    .then (data => {
                        dispatch ({
                            type: SPACE_IMAGES__REMOVING_SPACE_IMAGE_IN_SPACES_IMAGES_SUCCESS,
                            payload: data
                        });
                    });
                
            })
            .catch (err => {
                dispatch ( {
                    type: SPACE_IMAGES__REMOVING_SPACE_IMAGE_IN_SPACES_IMAGES_FAILED,
                    payload: err
                });
            });

    }
}