import {fetchFinanceHostPayments} from '../../API/finance';
import {FINANCE__FETCHING_HOSTPAYMENTS, FINANCE__FETCHING_HOSTPAYMENTS_SUCCESS, FINANCE__FETCHING_HOSTPAYMENTS_FAIL} from '../actionTypes/financeActionTypes'

export const getFinanceHostPayments = (page) => {
    return dispatch => {

        dispatch({
            type : FINANCE__FETCHING_HOSTPAYMENTS,
        });

        fetchFinanceHostPayments(page)
            .then(res => {
                dispatch({
                    type    : FINANCE__FETCHING_HOSTPAYMENTS_SUCCESS,
                    payload : res
                });
            })
            .catch(error => {
                dispatch({
                    type    : FINANCE__FETCHING_HOSTPAYMENTS_FAIL,
                    payload : error
                });
            });
    }
};
