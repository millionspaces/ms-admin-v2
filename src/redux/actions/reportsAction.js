import {    FETCHING_REPORTS, 
            FETCHING_BOOKINGSUMMARY_REPORT_SUCCESS,
            FETCHING_STATUS_REPORT_SUCCESS,
            FETCHING_REPORTS_FAILED,
            FETCHING_BOOKINGSUMMARY_BY_SPACE_REPORT_SUCCESS,
            FETCHING_BOOKINGSUMMARY_BY_USER_REPORT_SUCCESS,
            FETCHING_BOOKING_REPORT_SUCCESS,
            FETCHING_USER_REPORT_SUCCESS
         } from '../actionTypes/reportsActionTypes';
import {getReportDetails, getBookingReportDetails, getUserReportDetails} from '../../API/reports';
/**
 * Get report
 */

export const getBookingSummaryReportActionCreator = (report, path) => {

    return dispatch => {

        dispatch ({
            type: FETCHING_REPORTS
        });

        getReportDetails(path)
            .then(res => {
                dispatch ({
                    type: FETCHING_BOOKINGSUMMARY_REPORT_SUCCESS,
                    payload: res.data
                });
            })
            .catch(err => {
                dispatch ({
                    type: FETCHING_REPORTS_FAILED,
                    payload: err
                });
            });
    }
}

export const getStatusActionCreator = (report, path) => {

  return dispatch => {

      dispatch ({
          type: FETCHING_REPORTS
      });

      getReportDetails(path)
          .then(res => {
              dispatch ({
                  type: FETCHING_STATUS_REPORT_SUCCESS,
                  payload: res.data
              });
          })
          .catch(err => {
              dispatch ({
                  type: FETCHING_REPORTS_FAILED,
                  payload: err
              });
          });
    }
}

export const bookingSummaryBySpaceActionCreator = (path) => {

  return dispatch => {

      dispatch ({
          type: FETCHING_REPORTS
      });

      getReportDetails(path)
          .then(res => {
              dispatch ({
                  type: FETCHING_BOOKINGSUMMARY_BY_SPACE_REPORT_SUCCESS,
                  payload: res.data
              });
          })
          .catch(err => {
              dispatch ({
                  type: FETCHING_REPORTS_FAILED,
                  payload: err
              });
          });
    }
}
export const bookingSummaryByUserActionCreator = (path) => {

  return dispatch => {

      dispatch ({
          type: FETCHING_REPORTS
      });

      getReportDetails(path)
          .then(res => {
              dispatch ({
                  type: FETCHING_BOOKINGSUMMARY_BY_USER_REPORT_SUCCESS,
                  payload: res.data
              });
          })
          .catch(err => {
              dispatch ({
                  type: FETCHING_REPORTS_FAILED,
                  payload: err
              });
          });
    }
}

export const getBookingReportActionCreator = (path, startOfWeek, endOfWeek) => {

    return dispatch => {
  
        dispatch ({
            type: FETCHING_REPORTS
        });
  
        getBookingReportDetails(path, startOfWeek, endOfWeek)
            .then(res => {
                dispatch ({
                    type: FETCHING_BOOKING_REPORT_SUCCESS,
                    payload: res.data
                });
            })
            .catch(err => {
                dispatch ({
                    type: FETCHING_REPORTS_FAILED,
                    payload: err
                });
            });
      }
  }

export const getUserReportActionCreator = (path, startOfWeek, endOfWeek) => {
    return dispatch => {
  
        dispatch ({
            type: FETCHING_REPORTS
        });
  
        getUserReportDetails(path, startOfWeek, endOfWeek)
            .then(res => {
                dispatch ({
                    type: FETCHING_USER_REPORT_SUCCESS,
                    payload: res.data
                });
            })
            .catch(err => {
                dispatch ({
                    type: FETCHING_REPORTS_FAILED,
                    payload: err
                });
            });
      }
}