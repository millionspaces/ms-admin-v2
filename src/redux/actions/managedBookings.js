import { fetchManagedBookingsByPage, fetchABooking, cancelBooking, getManageBookingsByReferenceId} from "../../API/managedBookings.api";

import { 
    MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS_SUCCESS, 
    MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS, 
    MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS_FAILED, 
    MANAGED_BOOKINGS__FETCHING_A_BOOKING,
    MANAGED_BOOKINGS__FETCHING_A_BOOKING_SUCCESS,
    MANAGED_BOOKINGS__FETCHING_A_BOOKING_FAILED,
    MANAGED_BOOKINGS__CANCEL_A_BOOKING,
    MANAGED_BOOKINGS__CANCEL_A_BOOKING_SUCCESS,
    MANAGED_BOOKINGS__CANCEL_A_BOOKING_FAILED
} from "../actionTypes/managedBookingsActionTypes";

/**
 * Creates actions for retreving managed bookings
 * @param {string} pageNumber Page number
 */
 export const getMangedBookings = pageNumber => {
    return dispatch => {

        dispatch ({
            type: MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS
        });
        
        fetchManagedBookingsByPage (pageNumber)
            .then(res => res.json())
            .then(data => {
                dispatch ({
                    type: MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS_SUCCESS,
                    payload: data
                });
            })
            .catch(err => {
                dispatch ({
                    type: MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS_FAILED,
                    payload: err
                })
            });
    }
}

/**
 * get manageBookings by selectedId
 * @param {id} bookingId 
 */
export const getManageBookingsById = id => {
    return dispatch => {
        dispatch ({
            type: MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS
        });

        getManageBookingsByReferenceId(id)
            .then(res => res.json())
            .then(data => {
                dispatch ({
                    type: MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS_SUCCESS,
                    payload: {bookings: [{...data}]}
                });
            })
            .catch(err => {
                dispatch ({
                    type: MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS_FAILED,
                    payload: err
                })
            });

    }
}

/**
 * Creates actions for retreving managed bookings
 * @param {string} bookingId Page number
 */
export const getBookingDetailsById = bookingId => {

    return dispatch => {
        dispatch({
            type: MANAGED_BOOKINGS__FETCHING_A_BOOKING
        });

        fetchABooking (bookingId)
            .then (data => {


                dispatch ({
                    type: MANAGED_BOOKINGS__FETCHING_A_BOOKING_SUCCESS,
                    payload: data
                });
            })
            .catch(err => {
                dispatch ({
                    type: MANAGED_BOOKINGS__FETCHING_A_BOOKING_FAILED,
                    payload: err
                })
            });
    }
}


export const performCancelBooking = (bookingId)   => {

    return dispatch => {

        dispatch({
            type: MANAGED_BOOKINGS__CANCEL_A_BOOKING
        });

        cancelBooking(bookingId)
            .then(res => {
                if (res.status = 200) {
                    dispatch({
                        type: MANAGED_BOOKINGS__CANCEL_A_BOOKING_SUCCESS
                    });
                    dispatch(getMangedBookings(0))
                } else {
                    dispatch({
                        type: MANAGED_BOOKINGS__CANCEL_A_BOOKING_FAILED,
                        payload: 'Unhandled error'
                    });
                    dispatch(getMangedBookings(0))
                }
            })
            .catch(err => {

                dispatch({
                    type: MANAGED_BOOKINGS__CANCEL_A_BOOKING_FAILED,
                    payload:err
                });
                dispatch(getMangedBookings(0))
            });
    }
}