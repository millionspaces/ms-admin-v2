import {
    SPACES__FETCHING_SPACES_SET,
    SPACES__RECEIVING_SPACES_SET_SUCCESS,
    SPACES__RECEIVING_SPACES_SET_FAILED,
    SPACE__TOGGLING_SPACE_APPROVAL_STATE,
    SPACE__TOGGLING_SPACE_APPROVAL_STATE_SUCCESS,
    SPACE__TOGGLING_SPACE_APPROVAL_STATE_FAILED,
    SPACE__FETCHING_SPACE_DETAILS,
    SPACE__FETCHING_SPACE_DETAILS_SUCCESS,
    SPACE__FETCHING_SPACE_DETAILS_FAILED,
    SPACE_SAVE__SAVING_SPACE_DETAILS,
    SPACE_SAVE__SAVING_SPACE_DETAILS_SUCCESS,
    SPACE_SAVE__SAVING_SPACE_DETAILS_FAILED,
    SPACE__CLEAR_SPACE_DETAILS
} from '../actionTypes/spacesActionTypes';

import { fetchSpacesSet, toggleSpaceStatus, fetchSpaceById, updateSpaceDetails, getSpacesByReferenceId } from '../../API/spaces';

/**
 * Creates actions for receving spaces
 */
export const getSpacesSetActionCreator = page => {

    return dispatch => {

        dispatch ({
            type: SPACES__FETCHING_SPACES_SET
        });

        fetchSpacesSet(page)
            .then(res => res.json())
            .then(data => {
                dispatch ({
                    type: SPACES__RECEIVING_SPACES_SET_SUCCESS,
                    payload: data
                });
            })
            .catch(err => {
                dispatch ({
                    type: SPACES__RECEIVING_SPACES_SET_FAILED,
                    payload: err
                });
            });
    }
}
/**
 * Creates actions for revieve space for bookingId
 */
export const getSpacesForIdActionCreator = id => {
    return dispatch => {
        dispatch ({
            type: SPACES__FETCHING_SPACES_SET
        });

        getSpacesByReferenceId(id)
            .then(res => res.json())
            .then(data => {
                dispatch ({
                    type: SPACES__RECEIVING_SPACES_SET_SUCCESS,
                    payload: {spaces: [{
                        id: data.id,
                        name: data.name,
                        address: data.address,
                        organizationName: data.addressLine2,
                        approved: data.approved,
                        rating: data.rating,
                        hostName: data ? data.spaceOwnerDto.name : null
                    }]}
                });
            })
            .catch(err => {
                dispatch ({
                    type: SPACES__RECEIVING_SPACES_SET_FAILED,
                    payload: err
                });
            });
    }
}


/**
 * Creates actions for toggling space approval state
 */

export const toggleSpaceActiveStateActionCreator = id => {
    return dispatch => {
        dispatch ({
            type: SPACE__TOGGLING_SPACE_APPROVAL_STATE
        });
    
        toggleSpaceStatus(id)
            .then(res => res.json())
            .then(data => {
                dispatch ({
                    type: SPACE__TOGGLING_SPACE_APPROVAL_STATE_SUCCESS,
                    payload: {data, id}
                });
            })
            .catch(err => {
                dispatch ({
                    type: SPACE__TOGGLING_SPACE_APPROVAL_STATE_FAILED,
                    payload: err
                });
            });
    }

}

// TODO: Meaningfull name
/**
 * Creates actions to fetch space details by space id.
 */
export const getSpaceDetailsById = id => {
    return dispatch => {

        dispatch ({
            type: SPACE__FETCHING_SPACE_DETAILS
        });

        fetchSpaceById(id)
            .then( res =>  res.json())
            .then(data => {
                dispatch ({
                    type: SPACE__FETCHING_SPACE_DETAILS_SUCCESS,
                    payload: data
                });
            })
            .catch(err => {
                dispatch ({
                    type: SPACE__FETCHING_SPACE_DETAILS_FAILED
                })
            });

    }
}

/**
 * Creates actions to Edit a space
 * @param {Objct} space Space details
 */
export const updateSpaceActionCreator = space => {
    return dispatch => {
        dispatch ({
            type: SPACE_SAVE__SAVING_SPACE_DETAILS
        });

        updateSpaceDetails (space)
            .then(res => res.json)
            .then(data => {
                
                dispatch ({
                    type: SPACE__FETCHING_SPACE_DETAILS
                });
        
                fetchSpaceById(space.id)
                    .then( res =>  res.json())
                    .then(data => {
                        
                        dispatch ({
                            type: SPACE__FETCHING_SPACE_DETAILS_SUCCESS,
                            payload: data
                        });

                        dispatch ({
                            type: SPACE_SAVE__SAVING_SPACE_DETAILS_SUCCESS,
                            payload: data
                        });

                        dispatch(getSpaceDetailsById(space.id))
        
                    })
                    .catch(err => {
                        dispatch ({
                            type: SPACE__FETCHING_SPACE_DETAILS_FAILED
                        })
                    });
            })
            .catch(err => {
                dispatch ({
                    type: SPACE_SAVE__SAVING_SPACE_DETAILS_FAILED,
                    payload: err
                })
            });
    }
}

export const clearSpaceDetailsActionCreator = () => ({
    type: SPACE__CLEAR_SPACE_DETAILS
})