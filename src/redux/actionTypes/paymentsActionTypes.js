export const TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS = 'TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS';
export const TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS_SUCCESS = 'TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS_SUCCESS';
export const TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS_FAILED = 'TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS_FAILED';

export const TENTETIVE_BOOKINGS___CONFIRMING_TENTETIVE_BOOKING = 'TENTETIVE_BOOKINGS___CONFIRMING_TENTETIVE_BOOKING';
export const TENTETIVE_BOOKINGS___CONFIRMING_TENTETIVE_BOOKING_SUCCESS = 'TENTETIVE_BOOKINGS___CONFIRMING_TENTETIVE_BOOKING_SUCCESS';
export const TENTETIVE_BOOKINGS___CONFIRMING_TENTETIVE_BOOKING_FAILED = 'TENTETIVE_BOOKINGS___CONFIRMING_TENTETIVE_BOOKING_FAILED';