// Action Types
import { MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS, MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS_SUCCESS, MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS_FAILED, MANAGED_BOOKINGS__FETCHING_A_BOOKING, MANAGED_BOOKINGS__FETCHING_A_BOOKING_SUCCESS, MANAGED_BOOKINGS__FETCHING_A_BOOKING_FAILED, MANAGED_BOOKINGS__CANCEL_A_BOOKING, MANAGED_BOOKINGS__CANCEL_A_BOOKING_FAILED, MANAGED_BOOKINGS__RESET_STATE } from "../actionTypes/managedBookingsActionTypes";

export const managedBookingsReducer = (
    state={
        fetching: false,
        error: {status: false, message: ""},
        data: []
    }, action) => {
    switch (action.type) {
        case MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS:
            return Object.assign({}, state, {fetching: true});

        
        case MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS_SUCCESS:
            return Object.assign({}, {data: action.payload}, {fetching: false}, { error: {status: false, message: ""}});
        
        
        case MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS_FAILED:
            return Object.assign({},state, {data:[]}, {fetching: false}, {error: {status: true, message:"Failed to Fetch managed bookings"}});

        default:
            return state;
    }
}

export const currentBookingDetailsReducer = (
    state={
        fetching: false,
        error: {status: false, message: ""},
        data: []
    }, action) => {

    switch (action.type) {
        case MANAGED_BOOKINGS__FETCHING_A_BOOKING:
            return Object.assign({}, {fetching: true});

        case MANAGED_BOOKINGS__FETCHING_A_BOOKING_SUCCESS:
            return Object.assign ({}, {data: action.payload}, {fetching: false}, { error: {status: false, message: ""}});

        case MANAGED_BOOKINGS__FETCHING_A_BOOKING_FAILED:
            return Object.assign({}, {data: action.payload}, {fetching: false}, { error: {status: false, message: ""}});

        default:
            return state;
    }
}


const initStateCancelBooking = {success: false, error: false, fetching: false};

export const cancellBookingsReducer = (state=initStateCancelBooking, action) => {

    switch(action.type) {
        case MANAGED_BOOKINGS__CANCEL_A_BOOKING:
            return Object.assign({}, state, {fetching: true});

        case MANAGED_BOOKINGS__FETCHING_A_BOOKING_SUCCESS:
            return Object.assign({}, state, {fetching: false, success: false});

        case MANAGED_BOOKINGS__CANCEL_A_BOOKING_FAILED:
            return Object.assign({} , state, {fetching: false, error: action.payload, success: false});

        case MANAGED_BOOKINGS__RESET_STATE:
            return initStateCancelBooking;

        default:
            return state;
    }

}

