import { SPACE_STATISTICS_FETCHING, SPACE_STATISTICS_FETCH_SUCCESS, SPACE_STATISTICS_FETCH_FAIL} from '../actionTypes/statisticsActionTypes';

export const statistics = (state={fetching: false, error: {status: false, message:""}, data: []}, actions) => {
    switch(actions.type){
        case SPACE_STATISTICS_FETCHING:
            return Object.assign({}, state, {fetching: true});
        case SPACE_STATISTICS_FETCH_SUCCESS:
            return Object.assign({}, state , {fetching : false, data : actions.payload});
        case SPACE_STATISTICS_FETCH_FAIL:
            return Object.assign({}, state, {fetching: false, error:  {status : true, message : "Fail to load user statistic details"}})
        default :
            return state
    }
}

