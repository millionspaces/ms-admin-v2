import { TEMP_GET_USER_DETAILS, TEMP_GET_USER_DETAILS_FAILED, TEMP_GET_USER_DETAILS_SUCCESS } from "../actionTypes/tempActionTypes";

export const ChangeUserReducer = (state={fetching: false, error: false, user: null, success: false}, action) => {

    switch(action.type) {
        
        case TEMP_GET_USER_DETAILS:
            return Object.assign({}, state, {fetching: true, success: false});

        case TEMP_GET_USER_DETAILS_SUCCESS:
            return Object.assign({}, state, {fetching: false, error: false, user: action.payload});
      
        case 'CHANGING_USER_FAILED':
            return Object.assign({}, state, {error: true, fetching: false, success: false})

        case 'CHANGING_USER_SUCCESS':
            return Object.assign({}, state, {fetching: false, error: false, user: action.payload, success: true});

        case TEMP_GET_USER_DETAILS_FAILED:
            return Object.assign({}, state, {fetching: false, error: true, user: null });

        case 'RESET_USER_CHANGE':
            return {fetching: false, error: false, user: null, success: false}

        default:
            return state;
    }

}