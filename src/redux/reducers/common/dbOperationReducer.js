import { PERFORMING_DB_CREATION_OP, PERFORMING_DB_CREATION_OP_SUCCESS, PERFORMING_DB_CREATION_OP_FAILED, RESET_CREATION_STATE, PERFORMING_DB_DELETION_OP, PERFORMING_DB_DELETION_OP_SUCCESS, PERFORMING_DB_DELETION_OP_FAILED, RESET_DELETION_STATE, PERFORMING_DB_UPDATION_OP, PERFORMING_DB_UPDATION_OP_SUCCESS, RESET_UPDATION_STATE, PERFORMING_DB_UPDATION_OP_FAILED } from "../../actionTypes/common/commonActionTypes";

/**
 * Reducer for handling creation state portion.
 * @param {Object} state Auth state portion
 * @param {Object} action Action dispatched
 */
const initialCreationState = {creationState: null, error: null, message: '', fetching: false};

export const creationReducer = (state=initialCreationState, action) => {

    switch (action.type) {
        case PERFORMING_DB_CREATION_OP:
            return Object.assign({}, state, {fetching:true});

        case PERFORMING_DB_CREATION_OP_SUCCESS:
            return Object.assign({}, state, {creationState: 'success', fetching: false,  message: action.payload});

        case PERFORMING_DB_CREATION_OP_FAILED:
            return Object.assign({}, state, {creationState: 'failed', fetching: false, message: action.payload});

        case RESET_CREATION_STATE:
            return initialCreationState;

        default:
            return state;
    }
};

const initialDeletionState = {deletionState: null, error: null, message: '', fetching: false};

/**
 * Reducer for handling deletion state portion.
 * @param {Object} state Auth state portion
 * @param {Object} action Action dispatched
 */
export const deletionReducer = (state=initialDeletionState, action) => {

    switch (action.type) {
        case PERFORMING_DB_DELETION_OP:
            return Object.assign({}, state, {fetching: true});

        case PERFORMING_DB_DELETION_OP_SUCCESS:
            return Object.assign({}, state, {fetching: false, message: action.payload, deletionState: 'success'});

        case PERFORMING_DB_DELETION_OP_FAILED:
            return Object.assign({}, state, {deletionState: 'failed', fetching: false, message: action.payload});

        case RESET_DELETION_STATE:
            return Object.assign({}, initialDeletionState);

        default:
            return state;
    }
        
};

const initialUpdationState = {updationState: null, error: null, message: '', fetching: false};
/** 
 * Reducer that handling updation
 * @param {Object} state Auth state portion
 * @param {Object} action Action dispatched
 */
export const updationReducer = (state=initialUpdationState, action) => {

    switch(action.type) {

        case PERFORMING_DB_UPDATION_OP:
            return Object.assign({}, state, {fetching: true});

        case PERFORMING_DB_UPDATION_OP_SUCCESS:
            return Object.assign({}, state, {fetching: false, updationState: 'success', message: action.payload})

        case PERFORMING_DB_UPDATION_OP_FAILED:
            return Object.assign({}, state, {fetching: false, updationState: 'failed', message: action.payload})

        case RESET_UPDATION_STATE:
            return Object.assign({}, initialUpdationState);
            
        default:
            return state;
    }

};