import {
    AUTHENTICATION__ATTEMPTING_TO_LOGIN,
    AUTHENTICATION__LOGIN_SUCCESS,
    AUTHENTICATION__LOGIN_FAILED,
    AUTHENTICATION__ATTEMPTING_TO_LOGOUT,
    AUTHENTICATION__LOGOUT_SUCCESS,
    AUTHENTICATION__LOGOUT_FAILED, 
    AUTHENTICATION__LOGIN_RESET
} from '../actionTypes/authenticationActionTypes';

const initialState = { fetching: false, logged: false,  error: {status: false}};

export const userDeviceLoginReducer = (state=initialState, action) => {

    switch (action.type) {

        case AUTHENTICATION__ATTEMPTING_TO_LOGIN:
            return Object.assign({}, state, {fetching: true});

        case AUTHENTICATION__LOGIN_SUCCESS:
            return Object.assign({}, state, {fetching: false, logged: true}, action.payload);

        case AUTHENTICATION__LOGIN_FAILED:
            return Object.assign({}, {fetching: false}, {error: {status: true, message: action.payload}});

        case AUTHENTICATION__ATTEMPTING_TO_LOGOUT:
            return Object.assign({}, state, {fetching: true});

        case AUTHENTICATION__LOGOUT_SUCCESS:
            return Object.assign({}, { fetching: false, error: {status: false}});

        case AUTHENTICATION__LOGOUT_FAILED:
            return Object.assign({}, {fetching: false, logged: false}, state, {error: {status: true, message: action.payload}});
        
        case AUTHENTICATION__LOGIN_RESET:
            return initialState
        default:
            return state;
    }
}