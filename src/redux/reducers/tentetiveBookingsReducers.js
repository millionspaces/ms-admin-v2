import { TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS, TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS_SUCCESS, TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS_FAILED } from "../actionTypes/paymentsActionTypes";

export const tentetiveBookingsReducer = (
    state={
        fetching: false,
        error: {status: false, message: ""},
        data: []
    }, action
) => {
    switch (action.type) {
        case  TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS:
            return Object.assign({}, state, {fetching: true});

        case TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS_SUCCESS:
            return Object.assign({}, state, {fetching: false}, {data: action.payload});

        case TENTETIVE_BOOKINGS___FETCHING_TENTETIVE_BOOKINGS_FAILED:
            return Object.assign ({}, {data: []}, {fetching: false}, { error: {status: true, message: ""}});
            
        default:
            return state;
    }
}