import { TEMP_VERIFYING_CUSTOM_EMAIL, TEMP_VERIFYING_CUSTOM_EMAIL_SUCCESS, TEMP_VERIFYING_CUSTOM_EMAIL_FAILED, TEMP_RESET_MANUAL_EMAIL_VEFIF_STATE } from "../actionTypes/tempActionTypes";


const intialEmailVerif = {
    fetching: false,
    success: false,
    error: false
}

export const manualEmailVerificationReducer = (state=intialEmailVerif, action) => {

    switch(action.type) {

        case TEMP_VERIFYING_CUSTOM_EMAIL:
            return Object.assign({}, state, {fetching: true});

        case TEMP_VERIFYING_CUSTOM_EMAIL_SUCCESS:
            return Object.assign({}, state, {success: true, fetching: false, error: false});

        case TEMP_VERIFYING_CUSTOM_EMAIL_FAILED:
            return Object.assign({}, state, {error: true, fetching: false, success: false});

        case TEMP_RESET_MANUAL_EMAIL_VEFIF_STATE:
            return intialEmailVerif;

        default:
            return intialEmailVerif;
    }

}