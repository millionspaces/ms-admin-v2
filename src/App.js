import GAListener from 'components/GAListener';
import { EmptyLayout, LayoutRoute, MainLayout } from 'components/Layout';
import AlertPage from 'pages/AlertPage';
import AuthModalPage from 'pages/AuthModalPage';
import AuthPage from 'pages/AuthPage';
import BadgePage from 'pages/BadgePage';
import ButtonGroupPage from 'pages/ButtonGroupPage';
import ButtonPage from 'pages/ButtonPage';
import CardPage from 'pages/CardPage';
import ChartPage from 'pages/ChartPage';
// pages
import DashboardPage from 'pages/DashboardPage';
import DropdownPage from 'pages/DropdownPage';
import FormPage from 'pages/FormPage';
import InputGroupPage from 'pages/InputGroupPage';
import ModalPage from 'pages/ModalPage';
import ProgressPage from 'pages/ProgressPage';
import TablePage from 'pages/TablePage';
import TypographyPage from 'pages/TypographyPage';
import WidgetPage from 'pages/WidgetPage';
import React from 'react';
import componentQueries from 'react-component-queries';
import { BrowserRouter, Redirect, Switch } from 'react-router-dom';
import './styles/reduction.css';


// Custom components
// import Dashboard from './dashboard/Dashboard';
// import LoginModal from './login/LoginModal';
import PropTypes, { instanceOf } from 'prop-types';

import {userDeviceLogoutActionCreator} from './redux/actions/authenticationActions';

// Other
import {Cookies, withCookies } from 'react-cookie';
import { compose } from 'recompose';
import IdleTimer from 'react-idle-timer'

// Redux
import { connect } from 'react-redux';

const getBasename = () => {
  return `/${process.env.PUBLIC_URL.split('/').pop()}`;
};

class App extends React.Component {
  static propTypes = {
		cookies: instanceOf(Cookies).isRequired,
		id: PropTypes.number,
		role: PropTypes.string
	}

	state = {
		loggedIn: false,
		fetching: false
	};

  componentDidMount () {
		
		const { cookies } = this.props;

		cookies.get('MS_ADMIN_USER')?this.setState({loggedIn: true}):this.setState({loggedIn: false})

	}

	componentWillReceiveProps (nextProps) {

		const { id, fetching, role, cookies, logged } = nextProps;
        const {loggedIn} = this.state;
        if(this.state.fetching === false){
            if (fetching) {
                this.setState({
                    fetching
                });
            }
        }
		
        if(loggedIn === false){
            if (logged && id && role === 'USER') {
                cookies.set('MS_ADMIN_USER', id, {path:"/"});
                    this.setState({loggedIn: true});    
            }
        }

        if(loggedIn === true){
            if (!logged) {
                cookies.remove('MS_ADMIN_USER', {path: '/'});
                this.setState({ loggedIn: false, fetching });
            }
        }
		

	}

	onIdle = (e) => {
		const { fetching, cookies } = this.props;

		this.props.logout();
		cookies.remove('MS_ADMIN_USER', {path: '/'});
		this.setState({ loggedIn: false, fetching });
  } 
  
  render() {

    let { loggedIn, fetching } = this.state;
    return (
      <BrowserRouter basename={getBasename()}>
        <div>

          {loggedIn === true ?
            <Switch>
                <LayoutRoute
                exact
                path="/login"
                layout={EmptyLayout}
                component={props => (
                    <AuthPage {...props}/>
                )}
                />
                <LayoutRoute
                exact
                path="/signup"
                layout={EmptyLayout}
                component={props => (
                    <AuthPage {...props} />
                )}
                />
                <LayoutRoute
                exact
                path="/login-modal"
                layout={MainLayout}
                component={AuthModalPage}
                />
                <LayoutRoute
                exact
                path="/"
                layout={MainLayout}
                component={DashboardPage}
                />
                <LayoutRoute
                exact
                path="/buttons"
                layout={MainLayout}
                component={ButtonPage}
                />
                <LayoutRoute
                exact
                path="/cards"
                layout={MainLayout}
                component={CardPage}
                />
                <LayoutRoute
                exact
                path="/widgets"
                layout={MainLayout}
                component={WidgetPage}
                />
                <LayoutRoute
                exact
                path="/typography"
                layout={MainLayout}
                component={TypographyPage}
                />
                <LayoutRoute
                exact
                path="/alerts"
                layout={MainLayout}
                component={AlertPage}
                />
                <LayoutRoute
                exact
                path="/tables"
                layout={MainLayout}
                component={TablePage}
                />
                <LayoutRoute
                exact
                path="/badges"
                layout={MainLayout}
                component={BadgePage}
                />
                <LayoutRoute
                exact
                path="/button-groups"
                layout={MainLayout}
                component={ButtonGroupPage}
                />
                <LayoutRoute
                exact
                path="/dropdowns"
                layout={MainLayout}
                component={DropdownPage}
                />
                <LayoutRoute
                exact
                path="/progress"
                layout={MainLayout}
                component={ProgressPage}
                />
                <LayoutRoute
                exact
                path="/modals"
                layout={MainLayout}
                component={ModalPage}
                />
                <LayoutRoute
                exact
                path="/forms"
                layout={MainLayout}
                component={FormPage}
                />
                <LayoutRoute
                exact
                path="/input-groups"
                layout={MainLayout}
                component={InputGroupPage}
                />
                <LayoutRoute
                exact
                path="/charts"
                layout={MainLayout}
                component={ChartPage}
                />
                <LayoutRoute
                exact
                path="/register"
                layout={MainLayout}
                component={AuthPage}
                />
                <Redirect to="/" />
            </Switch>
          : <AuthPage/> }
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = ({auth}) => {

	const {id, role, fetching, name, logged} = auth;

	return {
		id,
		role,
		fetching,
		logged,
		name
	}
}

const mapDispatchToProps = dispatch => {
	return {
		logout : () => dispatch(userDeviceLogoutActionCreator())
	}
}

App = compose(
	connect(mapStateToProps, mapDispatchToProps)
) (App);

export default withCookies (App);

