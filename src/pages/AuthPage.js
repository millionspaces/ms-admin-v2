import AuthForm from 'components/AuthForm';
import React from 'react';
import { Card, Col, Row } from 'reactstrap';

// Redux
import { userDeviceLoginActionCreator } from '../redux/actions/authenticationActions';
import { connect } from 'react-redux';

class AuthPage extends React.Component {
  // handleAuthState = authState => {
  //   if (authState === STATE_LOGIN) {
  //     this.props.history.push('/login');
  //   } else {
  //     this.props.history.push('/signup');
  //   }
  // };

  handleLogoClick = () => {
    this.props.history.push('/');
  };

  handleLoginSubmit = (user) => {
    this.props.loginUserDevice(user)
  }

  render() {
    return (
      <Row
        style={{
          height: '100vh',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Col md={6} lg={4}>
          <Card body>
            <AuthForm
              handleLoginSubmit={this.handleLoginSubmit}
            />
          </Card>
        </Col>
      </Row>
    );
  }
}


const mapStateToProps = ({auth}) => {
  let {error} = auth;

  return {
      loginErrorStatus : error.status && error.status,
      message :  error.message && error.message
  }

};

const mapDispatchToProps = dispatch => {
  
  return {
      loginUserDevice: user=>dispatch(userDeviceLoginActionCreator(user))  
  }
}


export default connect(mapStateToProps, mapDispatchToProps) (AuthPage);